
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title>Lazada RTS Scanner</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
html{
    text-align:center;
}
body{
    text-align:center;
}
.item_box{
    text-align:left;
    width:70%;
}
form#get_order_items{
    max-width:500px;
    margin:0 auto;
    margin-top:50px;

}
form#get_order_items input{
    font-size:50px;
}
.form-control{
    height:auto;
    font-size:50px;
}
#number{
    pointer-events:none;
}
#wrap a{
    display:block;
    font-size:30px;
    display: block;
    border-radius: 25px;
    font-size: 15px;
    padding: 10px;
    border: 1px solid gray;
}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

</head>

<body>
<?php
ini_set("log_errors", 1);
error_reporting(E_ALL);
ini_set("error_log", "./logs/".date('Y-m-d')."shipment_create.log");

function print_label($url,$scanned_order_id){
    $writeto = dirname(__FILE__) . '/dump/'.$scanned_order_id.'.pdf';
    if(!file_exists($writeto)){
        //This is the file where we save the information
        file_put_contents($writeto, file_get_contents($url));

        shell_exec('print.bat ' . $scanned_order_id);

        echo '<div class="alert alert-success"><strong>Success</strong> AWB Generated and Sent to Printing...</div>';
    }else{
        file_put_contents($writeto, file_get_contents($url));

        shell_exec('print.bat ' . $scanned_order_id);

        echo '<div class="alert alert-warning"><strong>Warning:</strong> AWB Already Previously Printed!</div>';
    }
}

$item_list = "";
$hidden_order_number = "";
if (!empty($_POST)){
    $hidden_order_number = array_pop($_POST);
    $item_list = implode(",", $_POST);
    //print_r($item_list);
}
if($item_list){
    try {

        $url = file_get_contents('http://sync.synagie.com/btfl_lazada_scanner_malaysia/receiver.php?scanned_order_id='.$hidden_order_number.'&order_item_ids='.$item_list);
        //print($url);
        if (strpos($url, 'http') !== false){
            //echo "This is the url to pdf:".$url;
            print_label($url,$hidden_order_number);
        }else{
            echo '<div class="alert alert-danger"><strong>Error:</strong> '.$url.'</div>';
        }
    } catch (Exception $e) {
        error_log('Caught exception: ',  $e->getMessage(), "\n");
        echo '<div class="alert alert-danger"><strong>Error:</strong> Server Error!</div>';
    }
}
$hidden_order_number = ""; //enable this line if dun want to pull the same order.
?>
<form method="post" id="get_order_items">
<h1>Lazada RTS Scanner</h1>
<p>Please only scan the items *AFTER* QC is completed.<br>And only scan Simplypost Lazada Orders.</p>

  <div class="form-group">
    <label for="pwd">Scan Order Number</label>
    <input style="width:100%;float:left;" type="text" name="scanned_order_id" value="<?php echo $hidden_order_number; ?>" class="form-control" id="scanned_order_id"/>
    <!--<input style="width:15%;float:left;text-align:center;" type="text" name="number" class="form-control" id="number" value="1"/>
    <div id="wrap" style="width:10%;display:inline-block;float:left;">
        <a id="plus" href="#">
            <span class="glyphicon glyphicon-plus"></span>
        </a>
        <a id="minus" href="#">
            <span class="glyphicon glyphicon-minus"></span>
        </a>
    </div>-->
  </div>
  <button type="submit" class="btn btn-default">Retrieve Items</button>
</form>




<div class="container item_box" id="awb_submit"  style="display:none;" >
    <h3>Order Items</h3>
    <form method="post" >
        <table  class="table">
            <tr class="row box-clone">
                <td><input type="checkbox" id="checkAll" /> All</td>
                <td>No</td>
                <td>PLU</td>
                <td>Status</td>
            </tr>

            <tbody id="item_holder">
            </tbody>



        </table>
        <hr>
        <input type="hidden" name="hidden_order_number" id="hidden_order_number" value=""/>
        <div style="text-align:center;">
            <button type="submit" class="btn btn-default">Submit &amp; Print</button>
        </div>
    </form>

</div>

<script>
$(document).ready(function(){

    $('#scanned_order_id').focus();
    $('#number').change(function(){
        $('#scanned_order_id').focus();

    });
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
    $('#plus').click(function(){
        $('#number').val(parseInt($('#number').val()) + 1);
        $('#scanned_order_id').focus();
    });
    $('#minus').click(function(){
        if(parseInt($('#number').val()) > 1){
            $('#number').val(parseInt($('#number').val()) - 1);
            $('#scanned_order_id').focus();
        }
    });
    $('#get_order_items').submit(function(e){
        var item_list = [];
        e.preventDefault();
        $('#item_holder').html('loading...');
        $('#hidden_order_number').val($('#scanned_order_id').val());
        $.ajax({
            type: "GET",
            url: "http://sync.synagie.com/btfl_lazada_scanner_malaysia/get_order_items.php?scanned_order_id="+$('#scanned_order_id').val(),
            dataType: 'json',
            success: function(data_json){
                $('#awb_submit').show();
                $('#item_holder').html('');
                $.each(data_json, function(index, element) {
                    item_list.push(element);
                    var status = "not-printed";
                    if(element.tracking_code){
                        status = "printed";
                    }
                    if(item_list.includes("Order was cancelled at Lazada!")) {
                        $('#item_holder').append('<tr ><td colspan="4">The Order is Cancelled on Lazada</td></tr>');
                        return false;
                    }
                    if(!element.order_item_id){
                        $('#item_holder').append('<tr ><td colspan="4">Error in getting items. Please make sure order is completed.</td></tr>');
                        return false;
                    }
                    $('#item_holder').append('<tr class="row box-clone"><td><input type="checkbox" name="product_'+index+'" value="'+element.order_item_id+'"/></td><td>'+(index+1)+'.)</td> <td>'+element.name+' ('+element.order_item_id+')</td><td>'+status+'</td></tr>');
                });
                console.log(item_list);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $('#item_holder').html('');
                alert(errorThrown);
            }
        });
    });
    <?php
    if($hidden_order_number){
        echo "$('#get_order_items').submit();";
    }
    ?>
});
</script>
</body>
</html>
