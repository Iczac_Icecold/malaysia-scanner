<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Dosis:600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:600" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <title>Main Scanner Portal</title>
</head>
<?php
    $version = file_get_contents('.version');
?>
<style>

/*    Boostrap Override     */
    .modal-content  {
        -webkit-border-radius: 0px !important;
        -moz-border-radius: 0px !important;
        border-radius: 0px !important;
    }

    .admin-btn {
        height: 80px;
        width: 150px;
        margin-left: 16%;
        font-size: 20px;
    }

    .update-btn {
        height: 80px;
        width: 150px;
        margin-left: 16%;
        font-size: 20px;
    }

    .close-btn {
        height: 40px;
        width: 100px;
        font-size: 20px;
    }

    /*  Custom CSS  */

    #main_title {
        font-family: 'Nunito', sans-serif;
    }

    body {
        font-family: 'Dosis', sans-serif;
        font-size: 20px;
    }

    button:focus {
        outline:0;
    }
</style>
<body>
<div class="container">
    <div class="row">&nbsp;</div>
    <div class="row">
        <div class="col-md-12">
            <h1 id="main_title" style="text-align: center">Main Scanner Portal <a href="#" onclick="showChangeLog()" id="version" class="badge"><?=$version?></a></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="display: flex;justify-content: center">
            <div class="alert alert-success suclert" style="display: none" role="alert"></div>
            <div class="alert alert-danger daglert" style="display: none" role="alert"><b>Printer Change Failed</b></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h3>Current Printer - <span class="label label-info printer-label">New</span></h3>
        </div>
        <div class="col-md-4">
            <h3 style="text-align: center">Printers</h3>
            <div class="list-group">
                <button type="button" style="text-align: center" class="list-group-item gprinter"><b>G White Printer</b></button>
                <button type="button" style="text-align: center" class="list-group-item gblackprinter"><b>G Black Printer</b></button>
                <button type="button" style="text-align: center" class="list-group-item proprinter"><b>Pro Printer</b></button>
                <!-- <button type="button" style="text-align: center" class="list-group-item zprinter"><b>Zebra Printer</b></button> -->
                <!-- <button type="button" style="text-align: center" class="list-group-item sf"><b>SF Express</b></button> -->
            </div>
        </div>
        <div class="col-md-4">

<!--            Scanner List            -->

            <h3 style="text-align: center">Scanners</h3>
            <ul class="list-group">
              <a href="RTS_lazada_scanner_malaysia.php" target="_blank" class="list-group-item">
                  <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                  <b>Malaysia Lazada Scanner</b>
              </a>
                <!-- <a href="simplypost_scanner_create_envelope.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>After QC - Envelope Scanner</b>
                </a>
                <a href="simplypost_scanner_edit_shipment.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>After QC - Edit Scanner</b>
                </a>
                <a href="simplypost_scanner_create_shipment.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>After QC Scanner</b>
                </a>
                <a href="simplypost_scanner_create_next_day.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>Next Day Scanner</b>
                </a>
                <a href="sfexpress_scanner_create_shipment.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>SF Express After QC Scanner</b>
                </a>
                <a href="sfexpress_scanner_edit_shipment.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>SF Express Edit QC Scanner</b>
                </a>
                <a href="btfl_custom_label.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>Custom Label Scanner</b>
                </a>
                <a href="btfl_lazada_simplypost_label.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>Lazada SimplyPost Scanner</b>
                </a>
                <a href="imei_scanner.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>Lazada IMEI Scanner</b>
                </a>
                <a href="RTS_lazada_scanner.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>Lazada Multiple Parcel Scanner</b>
                </a> -->
                <!-- <a href="imei_test.php" target="_blank" class="list-group-item">
                    <span class="badge"><i class="glyphicon glyphicon-triangle-right"></i></span>
                    <b>Test Scanner</b>
                </a> -->
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <button id="admin" class="btn btn-danger">ADMIN MODE</button>
        </div>
    </div>
</div>

<!--    Admin Panel Modal   -->
<div id="adminModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #FF0022">
                <button type="button" class="close" id="close-x" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color:white;font-size: 20px;">You Are In Danger Zone</h4>
            </div>
            <div id="modal_body" class="modal-body">
                <div class="row">
                    <h4 style="text-align: center">Select Operation</h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button id="sync" class="btn btn-danger admin-btn">Sync</button>
                        <button id="repair" class="btn btn-warning admin-btn">Repair</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: flex;justify-content: center">
                <button type="button" id="close-me" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--    Admin Panel Ends    -->

<!--    Update Panel Modal  -->
<div id="updatePanel" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #5ac0de">
                <button type="button" class="close" id="close-x-update" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color:white;font-size: 20px;">New Version Available!!!!</h4>
            </div>
            <div id="update_modal_body" class="modal-body">
                <div class="row">
                    <h3 style="text-align: center">Do you want to update now?</h3>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button id="update_yes" class="btn btn-primary update-btn">Yes</button>
                        <button id="update_no" class="btn btn-warning update-btn">No</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: flex;justify-content: center">
                <button type="button" id="close-me-update" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--    Update Panel Ends  -->


<!--    Change Log Modal    -->
<div id="changeLogModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color:#5ac0de;font-size: 20px;">Synagie Scanner Version <span class="label label-info"><?=$version?></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <h2 style="text-align: center">ChangeLog - Version <?=$version?></h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Follow Below Template for Adding new Changelog -->
                        <h3>Scanner <?=$version?></h3>
                        <ul>
                            <li>New Printer Added
                                <ul>
                                    <li>TSC TTP-244 Pro Printer Added</li>
                                </ul>
                            </li>
                        </ul>
                        <br>
                        <h4>If you find any bugs, Please Contact <a href="mailto:iczac@btfl.ly">Us</a></h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: flex;justify-content: center">
                <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--    Change Log Modal Ends-->


</body>
<script>


//    Check Version Function
    function checkVersion() {
        var current_version = $('#version').html();

        $.ajax({
            url: "http://sync.synagie.com/project_oculus/malaysia_version.php",
            method: "GET",
            dataType: "json",
            success: function(result){

                if (result.version.trim() !== current_version.trim()) {
                    $('#close-me-update').prop('disabled', true);
                    $('#close-x-update').prop('disabled',true);
                    $('#updatePanel').modal('show');
                }
            }
        });

    }

//    Reload Count Down
    function countDown() {
        var now = 5;
        // Update the count down every 1 second
        var x = setInterval(function() {
            now = now - 1;

            // Output the result in an element with id="demo"
            document.getElementById("reload").innerHTML = now;

            // If the count down is over, write some text
            if (now === 0) {
                clearInterval(x);
                location.reload();
            }
        }, 1000);
    }


//    Check whether it's First time using it to show changelogs
    function firstTime() {
        if (localStorage.length < 1) {
            localStorage.setItem('before', true);
            showChangeLog();
        } else {
            if(!localStorage.getItem('before')) {
                localStorage.setItem('before', true);
                showChangeLog();
            }
        }
    }


//    Pathing changes depending on OS
    function getUrl(){
        if (navigator.platform == 'Win32') {
            return "/malaysia-scanner/PrinterController.php";
        } else if (navigator.platform == 'MacIntel') {
            return "/PrinterController.php";
        }
    }

    // Reset Admin Modal
    function resetModal(){
        $('#modal_body').html('<div class="row"> <h4 style="text-align: center">Select Operation</h4> </div> <div class="row"> <div class="col-md-12"> <button id="sync" class="btn btn-danger admin-btn">Sync</button> <button id="repair" class="btn btn-warning admin-btn">Repair</button> </div> </div>');
    }

    // Show Change Log Modal
    function showChangeLog() {
        $('#adminModal').modal('hide');
        $('#changeLogModal').modal('show');
    }

    // Getting Printer Update
    function getPrinter(){
        $.ajax({
            url: getUrl(),
            data: {name: 'init'},
            method: "POST",
            dataType: "json",
            success: function(result){
                var n = /(["'])(?:(?=(\\?))\2.)*?\1/g.exec(result.status);
                if (n[0] == '"Gprinter  GP-1324D"') {
                    var p_label = 'G White Printer';
                } else if (n[0] == '"Gprinter  GP-3120TU"') {
                    var p_label = 'G Black Printer';
                } else if (n[0] == '"TSC TTP-244 Pro"') {
                    var p_label = 'Pro Printer';
                } else if (n[0] == '"ZDesigner GK888t (EPL)"') {
                    var p_label = 'SF Printer';
                } else {
                    var p_label = 'Printer Not Found';
                }
                $('.printer-label').html(p_label);
            }
        })
    }

    $(document).ready(function(){

        // First time using to show change logs
        firstTime();

        // Get printer name
        getPrinter();

        //Check Version
        checkVersion();

        // Check Update Behaviour
        $('#update_yes').click(function (e) {
            e.preventDefault();
            $('#update_modal_body').html('<img style="margin-left: 27%;" src="assets/Bat.gif" alt="pacman"><h2 style="text-align: center">Fetching Data</h2>');

            $.ajax({
                url: getUrl(),
                data: {name: 'sync'},
                method: "POST",
                dataType: "json",
                success: function(result){
                    if (result.status == 'okay') {
                        $('#close-me-update').prop('disabled',true);
                        $('#close-x-update').prop('disabled',true);
                        countDown();
                        $('#update_modal_body').html('<h1 style="text-align: center;color:green;">Application Has Been Updated</h1>' +
                            '<p style="text-align: center">Page is Reloading in <span id="reload"></span></p>');
                        localStorage.clear();
                    } else {
                        $('#update_modal_body').html('<h1 style="text-align: center;color:red;">Update Failed: Please Contact Administrator</h1>');
                    }
                    getPrinter();
                }
            });
        });


        $('#update_no').click(function (e) {
            e.preventDefault();
            $('#updatePanel').modal('hide');
        });


        // Admin Modal On Close

        $('#adminModal').on('hidden.bs.modal', function () {
            resetModal();
        });


        // Admin Buttons
        $('#admin').click(function(e){
            e.preventDefault();
            $('#adminModal').modal('show');
        });

        $(document).on('click', '#sync', function(e){
            e.preventDefault();

            $('#modal_body').html('<img style="margin-left: 27%;" src="assets/Bat.gif" alt="pacman"><h2 style="text-align: center">Fetching Data</h2>');

            $.ajax({
                url: getUrl(),
                data: {name: 'sync'},
                method: "POST",
                dataType: "json",
                success: function(result){
                    if (result.status == 'okay') {
                        $('#close-me').prop('disabled',true);
                        $('#close-x').prop('disabled',true);
                        countDown();
                        $('#modal_body').html('<h1 style="text-align: center;color:green;">Application Has Been Updated</h1>' +
                            '<p style="text-align: center">Page is Reloading in <span id="reload"></span></p>');
                        localStorage.clear();
                    } else {
                        $('#modal_body').html('<h1 style="text-align: center;color:red;">Update Failed: Please Contact Administrator</h1>');
                    }
                    getPrinter();
                }
            });
        });

        $(document).on('click', '#repair', function(e){
            e.preventDefault();

            $('#modal_body').html('<img style="margin-left: 27%;" src="assets/Bat.gif" alt="pacman"><h2 style="text-align: center">Fetching Data</h2>');

            $.ajax({
                url: getUrl(),
                data: {name: 'repair'},
                method: "POST",
                dataType: "json",
                success: function(result){
                    if (result.status == 'okay') {
                        $('#close-me').prop('disabled',true);
                        $('#close-x').prop('disabled',true);
                        countDown();
                        $('#modal_body').html('<h1 style="text-align: center;color:green;">Application Has Been Repaired</h1><p style="text-align: center">Page is Reloading in <span id="reload"></span></p>');
                        localStorage.clear();
                    } else {
                        $('#modal_body').html('<h1 style="text-align: center;color:red;">Repair Failed: Please Contact Administrator</h1>');
                    }
                    getPrinter();
                }
            });
        });

        // Admin Buttons End
        $('.gprinter').click(function(e){
            e.preventDefault();
            $.ajax({
                url: getUrl(),
                data: {name: 'gprinter'},
                method: "POST",
                dataType: "json",
                success: function(result){
                    if (result.status == 'okay') {
                        $('.suclert').html('<b>G White Printer is Set</b>');
                        $('.daglert').css('display','none');
                        $('.suclert').css('display','block');
                    } else {
                        $('.suclert').css('display','none');
                        $('.daglert').css('display','block');
                    }
                    getPrinter();
                }
            });
        })

        $('.gblackprinter').click(function(e){
            e.preventDefault();
            $.ajax({
                url: getUrl(),
                data: {name: 'gblackprinter'},
                method: "POST",
                dataType: "json",
                success: function(result){
                    if (result.status == 'okay') {
                        $('.suclert').html('<b>G Black Printer is Set</b>');
                        $('.daglert').css('display','none');
                        $('.suclert').css('display','block');
                    } else {
                        $('.suclert').css('display','none');
                        $('.daglert').css('display','block');
                    }
                    getPrinter();
                }
            });
        })

        $('.proprinter').click(function(e){
            e.preventDefault();
            $.ajax({
                url: getUrl(),
                data: {name: 'proprinter'},
                method: "POST",
                dataType: "json",
                success: function(result){
                    if (result.status == 'okay') {
                        $('.suclert').html('<b>Pro Printer is Set</b>');
                        $('.daglert').css('display','none');
                        $('.suclert').css('display','block');
                    } else {
                        $('.suclert').css('display','none');
                        $('.daglert').css('display','block');
                    }
                    getPrinter();
                }
            });
        })
        

        $('.zprinter').click(function(e){
            e.preventDefault();
            $.ajax({
                url: getUrl(),
                data: {name: 'zprinter'},
                method: "POST",
                dataType: "json",
                success: function(result){
                    if (result.status == 'okay') {
                        $('.suclert').html('<b>Zebra Printer is Set</b>');
                        $('.daglert').css('display','none');
                        $('.suclert').css('display','block');
                    } else {
                        $('.suclert').css('display', 'none');
                        $('.daglert').css('display', 'block');
                    }
                    getPrinter();
                }
            });
        })

        $('.sf').click(function(e){
            e.preventDefault();
            $.ajax({
                url: getUrl(),
                data: {name: 'sfprinter'},
                method: "POST",
                dataType: "json",
                success: function(result){
                    if (result.status == 'okay') {
                        $('.suclert').html('<b>SF Printer is Set</b>');
                        $('.daglert').css('display','none');
                        $('.suclert').css('display','block');
                    } else {
                        $('.suclert').css('display', 'none');
                        $('.daglert').css('display', 'block');
                    }
                    getPrinter();
                }
            });
        })

    })
</script>
</html>
