
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title>After QC Scanner</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
html{
    text-align:center;
}
body{
    text-align:center;
}
form{
    max-width:500px;
    margin:0 auto;
    margin-top:200px;

}
form input{
    font-size:50px;
}
.form-control{
    height:auto;
    font-size:50px;
}
#number{
    pointer-events:none;
}
#wrap a{
    display:block;
    font-size:30px;
    display: block;
    border-radius: 25px;
    font-size: 15px;
    padding: 10px;
    border: 1px solid gray;
}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

</head>

<body>
<form action="" method="post">
<?php
include('./fpdf/fpdf.php'); 
include('./fpdi/fpdi.php'); 
// initiate FPDI 

function writePDF($file_name,$count){

    $pdf = new FPDI('P','pt',array(290,450)); 

    for ($x = 1; $x <= $count; $x++) {
        // add a page 
        $pdf->AddPage(); 
        // set the sourcefile 
        $pdf->setSourceFile($file_name); 
        // import page 1 
        $tplIdx = $pdf->importPage(1); 
        // use the imported page as the template 
        $pdf->useTemplate($tplIdx, 0, 0); 

        // now write some text above the imported page 
        $pdf->SetFont('Arial','B',10); 
        $pdf->SetTextColor(0,0,0); 
        $pdf->SetXY(156, 176); 
        if($count > 1){
            $pdf->Write(0, $x." OF"); 
        }
        
    } 
    $pdf->Output($file_name, 'F');
}

if (isset($_POST['scanned_order_id'])){
	$scanned_order_id = $_POST['scanned_order_id'];
	$no_of_pack = $_POST['number'];
} else {
	$scanned_order_id = '';
	$no_of_pack = 0;
}


if($scanned_order_id && $no_of_pack){

    $scanned_order_id = urlencode($scanned_order_id);
    $tracking_no = file_get_contents("http://staging.zension.net/btfl_simplypost_scanner/simplypost_get_tracking_no_v2.php?order_id=".$scanned_order_id.'&no_of_package='.$no_of_pack);


    if($tracking_no){

        $url = "https://ws01.ffdx.net/v4/printdoc/docConnoteStyle2.aspx?qr=1&accessid=B1D76927CB6895FDA443DDD5DC64AB20&format=pdf&item=m&shipno=".$tracking_no;

        $shipment_no = $tracking_no;

        set_time_limit(0);
        //print_r($url);
        $writeto = dirname(__FILE__) . '/dump/'.$shipment_no . '_'.$scanned_order_id.'.pdf';
        if(!file_exists($writeto)){
            //This is the file where we save the information
            file_put_contents($writeto, file_get_contents($url));


           // writePDF($writeto,(int)$no_of_pack);
		shell_exec('print.bat ' . $shipment_no . '_' . $scanned_order_id);

            echo '<div class="alert alert-success"><strong>Success</strong> AWB Generated and Sent to Printing...</div>';
        }else{
            file_put_contents($writeto, file_get_contents($url));

            //writePDF($writeto,(int)$no_of_pack);
		shell_exec('print.bat ' . $shipment_no . '_' . $scanned_order_id);
            
            echo '<div class="alert alert-warning"><strong>Warning:</strong> AWB Already Previously Printed! Please check again.</div>';
        }
    }else{
        echo '<div class="alert alert-danger"><strong>Error:</strong> AWB Not Found for This Order? Please check!</div>';
    }
}
?>
<h1>After QC Scanner</h1>
<p>Please only scan the items *AFTER* QC is completed.</p>
<form method="post" name="myform">
  <div class="form-group">
    <label for="pwd">Scan Order Number</label>
    <input style="width:75%;float:left;" type="text" name="scanned_order_id" class="form-control" id="scanned_order_id"/>
    <input style="width:15%;float:left;text-align:center;" type="text" name="number" class="form-control" id="number" value="1"/>
    <div id="wrap" style="width:10%;display:inline-block;float:left;">
        <a id="plus" href="#">
            <span class="glyphicon glyphicon-plus"></span>
        </a>
        <a id="minus" href="#">
            <span class="glyphicon glyphicon-minus"></span>
        </a>
    </div>
  </div>
  <button type="submit" class="btn btn-default">Retrieve &amp; Print AWB</button>
</form>
<script>
$(document).ready(function(){
    $('#scanned_order_id').focus();
    $('#number').change(function(){
        $('#scanned_order_id').focus();

    });
    $('#plus').click(function(){
        $('#number').val(parseInt($('#number').val()) + 1);
        $('#scanned_order_id').focus();
    });
    $('#minus').click(function(){
        if(parseInt($('#number').val()) > 1){
            $('#number').val(parseInt($('#number').val()) - 1);
            $('#scanned_order_id').focus();
        }
    });
});
</script>
</body>
</html>


