<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title>After QC Scanner</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
html{
    text-align:center;
}
body{
    text-align:center;
}
form{
    max-width:500px;
    margin:0 auto;
    margin-top:200px;

}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

</head>

<body>
<form action="" method="post">
<?php
if (isset($_POST['scanned_order_id'])) {
$scanned_order_id = $_POST['scanned_order_id'];
	if($scanned_order_id ){
		$tracking_no = file_get_contents("http://staging.zension.net/skuvault_sync_nestle/simplypost_get_tracking_no.php?order_id=".$scanned_order_id);


		if($tracking_no){

			$url = "https://ws01.ffdx.net/v4/printdoc/docConnoteStyle2.aspx?qr=1&accessid=B1D76927CB6895FDA443DDD5DC64AB20&format=pdf&shipno=".$tracking_no;

			$shipment_no = $tracking_no;

			set_time_limit(0);

			$writeto = dirname(__FILE__) . '/dump/'.$shipment_no . '_'.$scanned_order_id.'.pdf';
			if(!file_exists($writeto)){
				/*//This is the file where we save the information
				$fp = fopen ($writeto, 'w+');
				//Here is the file we are downloading, replace spaces with %20
				$ch = curl_init(str_replace(" ","%20",$url));
				curl_setopt($ch, CURLOPT_TIMEOUT, 50);
				// write curl response to file
				curl_setopt($ch, CURLOPT_FILE, $fp); 
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				// get curl response
				curl_exec($ch); 
				curl_close($ch);
				fclose($fp);*/
				file_put_contents($writeto, fopen($url, 'r'));
				shell_exec('print.bat ' . $shipment_no . '_' . $scanned_order_id);


				echo '<div class="alert alert-success"><strong>Success</strong> AWB Generated and Sent to Printing...</div>';
			}else{
  				shell_exec('print.bat ' . $shipment_no . '_' . $scanned_order_id);
				echo '<div class="alert alert-warning"><strong>Warning:</strong> AWB Already Previously Printed! Please check again.</div>';
			}
		}else{
			echo '<div class="alert alert-danger"><strong>Error:</strong> AWB Not Found for This Order? Please check!</div>';
		}
	}	
}

?>
<h1>After QC Scanner</h1>
<p>Please only scan the items *AFTER* QC is completed.</p>
<form method="post" name="myform">
  <div class="form-group">
    <label for="pwd">Scan Order Number</label>
    <input type="text" name="scanned_order_id" class="form-control" id="scanned_order_id">
  </div>
  <button type="submit" class="btn btn-default">Retrieve &amp; Print AWB</button>
</form>
<script>
$(document).ready(function(){
    $('#scanned_order_id').focus();
});
</script>
</body>
</html>


